import numpy as np
import matplotlib.pylab as plt
import matplotlib.animation as animation
#from IPython.display import HTML

plt.rcParams['animation.ffmpeg_path'] = '/usr/bin/ffmpeg'

np.random.seed()

N = 30      # number of particles
m0 = 1      # mass coefficient
r0 = 0.05   # radius coefficient
dt = 0.001  # time step in drawing animation
time = 0    # time counter between events
dd = 0      # time to the nearest event

fig, ax = plt.subplots(figsize=(6,6))
plt.xlabel(r'$x$')
plt.ylabel(r'$y$')
#plt.close() ###########

ax.set_xlim((0, 1))
ax.set_ylim((0, 1))
particles = ax.scatter([], [], [], edgecolors='b', facecolors='none')

def next_event():
    '''
    event=0 - particle hits vertical wall
    event=1 - particle hits horizontal wall
    event=2 - particle hits other particle
    '''
    deltat = 1.e10
    particle2 = 0
    for i in np.arange(N):
        if vx[i] > 0: 
            new_deltat = (1-r[i]-x[i])/vx[i]
        elif vx[i] < 0: 
            new_deltat = (r[i]-x[i])/vx[i]
        else: new_deltat = 1.e10
        if new_deltat < deltat:
            event = 0
            particle1 = i          # the number of the particle which hits the wall
            deltat = new_deltat
            
        if vy[i] > 0: 
            new_deltat = (1-r[i]-y[i])/vy[i]
        elif vy[i] < 0: 
            new_deltat = (r[i]-y[i])/vy[i]
        else: new_deltat = 1.e10
        if new_deltat < deltat:
            event = 1
            particle1 = i
            deltat = new_deltat
            
        for j in np.arange(i+1,N):
            d = r[i] + r[j]
            deltar = [x[j] - x[i], y[j] - y[i]]
            deltav = [vx[j] - vx[i], vy[j] - vy[i]]
            deltav_deltar = np.dot(deltav,deltar)
            deltav_deltav = np.dot(deltav,deltav)
            deltar_deltar = np.dot(deltar,deltar)
            if deltav_deltar >= 0:
                new_deltat = 1.e10
                continue
            else:
                DD = deltav_deltar**2 - deltav_deltav \
                * (deltar_deltar - d**2)
                if DD < 0:
                    new_deltat = 1.e10
                    continue
                else:
                    new_deltat = -(deltav_deltar + np.sqrt(DD))/deltav_deltav
                    if new_deltat < 0: new_deltat = 1.e10
                    
            if new_deltat < deltat :
                event = 2
                particle1 = i # the number of the 1st colliding particle 
                particle2 = j # the number of the 2nd colliding particle
                deltat = new_deltat        
                # returns Delta t, type of event and particle(s) taking part in the event
    return deltat,event,particle1,particle2

def resolve_collision(event,particle1,particle2):
    global x,y,vx,vy,m,r
    if event == 0: vx[particle1] = -vx[particle1]
    if event == 1: vy[particle1] = -vy[particle1]
    if event == 2:
        delta_x = x[particle2]-x[particle1]
        delta_y = y[particle2]-y[particle1]
        deltav_deltar = (vx[particle2]-vx[particle1])*delta_x + (vy[particle2]-vy[particle1])*delta_y
        d = r[particle1] + r[particle2]
        J = 2*m[particle1]*m[particle2]*deltav_deltar/(d*d*(m[particle1]+m[particle2]))
        Jx = J*delta_x
        Jy = J*delta_y
        vx[particle1] += Jx/m[particle1]
        vx[particle2] -= Jx/m[particle2]
        vy[particle1] += Jy/m[particle1]
        vy[particle2] -= Jy/m[particle2]
 
def init():
    global x,y,vx,vy,m,r,dd,ee,p1,p2,time
    r = r0*(0.8*np.random.rand(N)+0.2) #*np.ones(N)
    x = []
    y = []
    x = np.append(x,(1-2*r[0])*np.random.rand()+r[0]) # to avoid an overlap with the boundaries 
    y = np.append(y,(1-2*r[0])*np.random.rand()+r[0])
    for i in np.arange(1,N):
        # to avoid an overlap between different discs
        overlap = True
        while overlap :
            new_x = (1-2*r[i])*np.random.rand()+r[i]
            new_y = (1-2*r[i])*np.random.rand()+r[i]
            overlap = False
            for j in np.arange(i):
                overlap = overlap or (new_x-x[j])**2+(new_y-y[j])**2 <= (r[i]+r[j])**2
        x = np.append(x,new_x)
        y = np.append(y,new_y)
    vx = 2*np.random.rand(N)-1 # components of the velocity random from -1 to 1
    vy = 2*np.random.rand(N)-1
    m = r*r
    deltat = 1.e10
    particle1 = 0
    particle2 = 0
    XY = np.c_[x, y]
    particles.set_offsets(XY)
    particles.set_sizes(400000*r*r)
    time = 0
    dd,ee,p1,p2=next_event()
    return particles,

def animate(j):
    global x,y,vx,vy,time,dd,ee,p1,p2
    if time < dd:
        xx = x + vx*time
        yy = y + vy*time
        XY = np.c_[x, y]
        particles.set_offsets(XY)
        time += dt
    else:
        x += vx*dd
        y += vy*dd
        resolve_collision(ee,p1,p2)
        dd,ee,p1,p2=next_event()
        XY = np.c_[x, y]
        particles.set_offsets(XY)
        time = 0
    return particles,


#init()
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=5000, interval=5, blit=True, repeat=True)        
#anim.save("gas.mp4")
plt.show()
#HTML(anim.to_jshtml())
